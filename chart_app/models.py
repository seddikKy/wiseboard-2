from django.db import models
import random
from django.db import models, transaction


class Product(models.Model):
    designation = models.CharField(max_length=100, unique=True)

    @staticmethod
    def populate_initial_data():
        """
        Populate products table with sample data
        """
        products = [
            'AA',
            'BB',
            'CC',
            'DD',
            'EE',
            'FF',
            'GG',
            'HH',
            'II',
            'JJ',
            'KK',
        ]
        with transaction.atomic():
            for product in products:
                Product(
                    id=products.index(product) + 1,
                    designation=product
                ).save()

    def __str__(self):
        return self.designation


class Sale(models.Model):
    product = models.ForeignKey(Product, on_delete=models.CASCADE)
    quantity = models.PositiveIntegerField()
    transaction_date = models.DateField()

    @staticmethod
    def generate_random_date():
        import datetime

        start_date = datetime.date(2020, 1, 1)
        end_date = datetime.date(2020, 12, 1)

        time_between_dates = end_date - start_date
        days_between_dates = time_between_dates.days
        random_number_of_days = random.randrange(days_between_dates)
        random_date = start_date + datetime.timedelta(days=random_number_of_days)

        return random_date

    def populate_random_data(self):
        """
        Populate sales table with random data
        """

        with transaction.atomic():
            for i in range(10000):
                Sale(
                    product_id=random.randint(1, 11),
                    quantity=random.randint(1, 1500),
                    transaction_date=self.generate_random_date()

                ).save()
