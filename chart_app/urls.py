from django.urls import path

from . import views

urlpatterns = [

    path('', views.DashExample.as_view(), name='dashboard'),
    path(views.SalesChart.url, views.SalesChart.as_view(), name='sales_chart'),
    path(views.SalesChartBars.url, views.SalesChartBars.as_view(), name='sales_chart'),
    path('dashboard', views.DashExample.as_view(), name='dashboard'),
]
